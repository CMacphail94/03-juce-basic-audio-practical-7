//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 18/11/2014.
//
//

#include "SinOscillator.h"

SinOscillator::SinOscillator()
{

    phaseIncrement = (twoPi * oscFreq)/sampleRate;
    phasePosition = 0.f;
    
}

SinOscillator::~SinOscillator()
{
    
}

float SinOscillator::getSample()
{

    phaseIncrement = (twoPi * oscFreq)/sampleRate;
    phasePosition += phaseIncrement;
    
    
    
    if(phasePosition > twoPi)
    {
        
        phasePosition -= twoPi;
    }
    
    
    return sin(phasePosition) * oscAmp;
    
}

void SinOscillator::setFrequency(float frequency)
{
    oscFreq = frequency;
    std::cout << "osc freq set\n";
}

void SinOscillator::setAmplitude(float amplitude)
{
    oscAmp = amplitude;
    std::cout << "osc amp set\n";
}