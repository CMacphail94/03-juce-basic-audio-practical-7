//
//  SinOscillator.h
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 18/11/2014.
//
//

#ifndef __JuceBasicAudio__SinOscillator__
#define __JuceBasicAudio__SinOscillator__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"

class SinOscillator

{
public:
    
    /** Constructor */
    SinOscillator();
    
    /** Destructor */
    ~SinOscillator();
    
    float getSample();
    void setFrequency(float frequency);
    void setAmplitude(float amplitude);
    
private:
    float oscFreq;
    float oscAmp;
    float sampleRate = 44100;
    float phasePosition;
    float twoPi = 2 * M_PI;
    float phaseIncrement;
    
    
};

#endif /* defined(__JuceBasicAudio__SinOscillator__) */
