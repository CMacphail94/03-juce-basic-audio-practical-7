/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    SinOscillator();
    
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    std::cout << "midi input\n";
    if(message.isNoteOn())
    {
        std::cout << "note on\n";
        sharedMemory.enter();
        amplitude = 1.0;
        sinOscillator.setAmplitude(amplitude);
        frequency = juce::MidiMessage::getMidiNoteInHertz(message.getNoteNumber());
        sinOscillator.setFrequency(frequency);
        sharedMemory.exit();
    }
    
    if(message.isNoteOff())
    {
        sharedMemory.enter();
        amplitude = 0.0;
        sinOscillator.setAmplitude(amplitude);
        sharedMemory.exit();
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    

    
    
    while(numSamples--)
    {

        
        *outL = sinOscillator.getSample();
        *outR = sinOscillator.getSample();
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    
    sampleRate = device->getCurrentSampleRate();
    
    
    
}

void Audio::audioDeviceStopped()
{

}